package library;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Library {

    static Connection connection = null;

    public static void main(String[] args) {

        try {
            ConnectClass.connect();

            System.out.println("Erzsike tessék kiválasztani, hogy mit szeretne");
            System.out.println("1 es gomb, új felhasználó létrehozás");
            System.out.println("2 es gomb, kölcsönzés dokumentálása");
            System.out.println("3 as gomb, visszahozott könyv dokumentálása");
            System.out.println("4 es gomb, könyv hozzáadása");
            Scanner sc = new Scanner(System.in);
            int input = sc.nextInt();
            if (input == 1) {
                addNewUser();
            }
            if (input == 2) {
                addNewRent();
            }
            if (input == 3) {
                returnBook();
            }
            if(input == 4){
                addBook();
            }
            if (input < 1 || input > 4) {
                System.out.println("Hibás input");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static void addNewUser() throws SQLException {
        System.out.println("Írja be (enterrel elválasztva : az új felhasználó "
                + "nevét, e-mail címét ");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        String email = sc.nextLine();
        String string = "insert into user(Registration_date,Name,Email) values (?,?,?)";
        PreparedStatement ps = connection.prepareStatement(string);
        ps.setTimestamp(1, new java.sql.Timestamp(new java.util.Date().getTime()));
        ps.setString(2, name);
        ps.setString(3, email);
        ps.execute();
    }

    private static void addNewRent() {
        //TODO Balázs
    }

    private static void returnBook() {
        //TODO Ati
    }

    private static void addBook() {
        //TODO MÁTÉ
    }

}
