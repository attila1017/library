package library;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Scanner;

public class ConnectClass {

    public static void connect() throws SQLException, FileNotFoundException {

        Scanner sc = new Scanner(new File("src/pw.txt"));
        String user = sc.nextLine();
        String pw = sc.nextLine();

        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName("localhost");
        ds.setPort(3306);
        ds.setDatabaseName("Library");
        ds.setUser(user);
        ds.setPassword(pw);
        Library.connection = ds.getConnection();

    }

}
